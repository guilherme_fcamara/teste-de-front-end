import {telMask} from "./telmask";

describe ('Component: FormInputComponent', () => {
    it('should mask Telephone', () => {
        let tel = "13999999999";
        expect(telMask(tel)).toEqual('(13) 99999-9999');
    })
})