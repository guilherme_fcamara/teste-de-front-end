export function telMask(tel){
    tel=tel.replace(/\D/g,"");             
    tel=tel.replace(/^(\d{2})(\d)/g,"($1) $2");
    tel=tel.replace(/(\d)(\d{4})$/,"$1-$2");
    return tel;
}