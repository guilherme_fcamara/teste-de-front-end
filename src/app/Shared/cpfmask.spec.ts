import {cpfMask} from "./cpfmask";

describe ('Component: FormInputComponent', () => {
    it('should mask cpf', () => {
        let cpf = "11111111111";
        expect(cpfMask(cpf)).toEqual('111.111.111-11');
    })
})