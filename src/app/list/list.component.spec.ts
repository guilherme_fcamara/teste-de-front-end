import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListComponent } from './list.component';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListComponent ]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(ListComponent);
      component = fixture.componentInstance;

    });
  }));

  it('should create the app', () => {
    let fixture = TestBed.createComponent(ListComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should call loadLocalStorage', () => {
    var spy = spyOn(component, "loadLocalStorage");
    fixture.detectChanges();
    expect(component.loadLocalStorage).toHaveBeenCalled();
  })
});
