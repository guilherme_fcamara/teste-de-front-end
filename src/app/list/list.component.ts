import { Component, OnInit } from '@angular/core';
import { User } from '../Models/user.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  storageUsers = [];

  constructor() { }

  ngOnInit(): void {
    this.storageUsers = JSON.parse(localStorage.getItem('Usuarios'));
    this.loadLocalStorage();
  }

  loadLocalStorage(){
    if(this.storageUsers.length > 0){
      this.storageUsers.forEach((user, i) => {
        let userItem: User = user;
      })
    }
  }
  refreshList(newItem: string){
    this.ngOnInit();
  }   

}
