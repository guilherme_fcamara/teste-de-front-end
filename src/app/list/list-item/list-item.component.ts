import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../Models/user.model';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }
  @Input() user: User;
  @Input() index: string;
  @Output() refresh = new EventEmitter<string>();

  removeItem(index){
    let users = JSON.parse(localStorage.getItem('Usuarios'));
    users.splice(index, 1);
    localStorage.setItem('Usuarios', JSON.stringify(users));

    this.refresh.emit("true");
  }
}
