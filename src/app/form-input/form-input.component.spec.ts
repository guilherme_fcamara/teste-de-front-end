import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormInputComponent } from './form-input.component';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

describe('FormInputComponent', () => {
  let component: FormInputComponent;
  let fixture: ComponentFixture<FormInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, FormsModule, ReactiveFormsModule],
      declarations: [ FormInputComponent ]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(FormInputComponent);
      component = fixture.componentInstance;

    });;
  }));

  it('should create', () => {
    const fixture = TestBed.createComponent(FormInputComponent);
    const app = fixture.componentInstance;
    expect(component).toBeTruthy();
  });

});
