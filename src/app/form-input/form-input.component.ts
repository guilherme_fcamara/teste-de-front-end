import { Component, Input ,OnInit } from '@angular/core';
import { User } from '../Models/user.model';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { cpfMask } from '../Shared/cpfmask';
import { telMask } from '../Shared/telmask';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.css']
})
export class FormInputComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  constructor(private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      nome: ['', Validators.required],
      cpf: ['', [Validators.required, Validators.pattern(/^(([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2})|([0-9]{11}))$/)]],
      telephone: ['', [Validators.required, this.validateTel]],
      email: ['', [Validators.required, Validators.email]]
    });
  }
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
        return;
    }
    this.saveUser();
  }

  onKeypressCPF(event: Event){
    (<HTMLInputElement>event.target).value =  cpfMask((<HTMLInputElement>event.target).value);
  }

  onKeypressTel(event: Event){
    (<HTMLInputElement>event.target).value =  telMask((<HTMLInputElement>event.target).value);
  }

  validateTel (control: FormControl) {
    let numeroTel = control.value.replace(")", "").replace("(", "").replace("-", "").replace(" ", "");
    let regex = new RegExp('^((1[1-9])|([2-9][0-9]))((3[0-9]{3}[0-9]{4})|(9[0-9]{3}[0-9]{5}))$');
    if(regex.test(numeroTel)){
    }else{
        return {
          wrongpattern: control.value
        }
    }
  }

  saveUser(){
    let user = new User(this.f["nome"].value, this.f["cpf"].value, this.f["telephone"].value, this.f["email"].value);
    console.log("UHuu "+JSON.stringify(user));
    let storageUsers = JSON.parse(localStorage.getItem('Usuarios'));

    if(storageUsers){
        storageUsers.push(user);
    }else{
        storageUsers = [];
        storageUsers.push(user);
    }
    localStorage.setItem("Usuarios", JSON.stringify(storageUsers));
    this.router.navigate(['/list']);

  }
}
