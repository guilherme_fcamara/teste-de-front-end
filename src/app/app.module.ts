import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { AppComponent } from './app.component';
import { FormInputComponent } from './form-input/form-input.component';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { ListItemComponent } from './list/list-item/list-item.component';

const appRoutes: Routes = [
  {path: '', component: FormInputComponent},
  {path: 'form', component: FormInputComponent},
  {path: 'list', component: ListComponent}
  
];

@NgModule({
  declarations: [
    AppComponent,
    FormInputComponent,
    ListComponent,
    ListItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
